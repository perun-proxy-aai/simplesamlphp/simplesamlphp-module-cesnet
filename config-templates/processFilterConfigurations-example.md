## ComputeLoA

Example how to configure ComputeLoA filter:

- Put something like this into saml20-idp-hosted.php:

  ```php
  11 => [
          'class' => 'cesnet:ComputeLoA',
  ],
  ```

## IsCesnetEligible

Example how to configure IsCesnetEligible filter:

- Interface says if attribute will be read from RPC or LDAP
- If interface is LDAP, LDAP.attributeName has to be filled
- RPC.attributeName has to be filled
- Put something like this into saml20-idp-hosted.php:

  ```php
  25 => [
      'class' => 'cesnet:IsCesnetEligible',
      'interface' => 'RPC/LDAP',
      'RPC.attributeName' => 'urn:perun:user:attribute-def:def:isCesnetEligibleLastSeen',
      'LDAP.attributeName' => 'isCesnetEligible',
  ],
  ```

## IsEinfraCZEligible

Example how to configure IsEinfraCZEligible filter:

- Configuration file is saml20-idp-hosted.php
- User eligibilites and affiliations should be retrieved before this filter is run
- User ext. sources are not updated as part of this filter

  ```php
  33 => [
      'class' => 'cesnet:IsEinfraCZEligible',
      'userEligibilityAttrName' => 'session_eligibilities',
      'userAffiliationAttrName' => 'externalAffiliation',
      'eligibilitiesAttrKey' => 'einfracz',
  ],
  ```

## IsEinfraAssured

Example how to configure IsEinfraAssured filter:

- Configuration file is saml20-idp-hosted.php
- Einfracz eligibility should be resolved before this filter is run
- User eligibilities and session eligibilities should be retrieved before this filter is run
- The result values are stored in attributes defined in `userAssuranceAttrNames` as value `assurancePrefix-1y`

  ```php
  34 => [
      'class' => 'cesnet:IsEinfraAssured',
      'userAssuranceAttrNames' => ['eduPersonAssurance'],
      'userEligibilitiesAttrName' => 'eligibilities',
      'sessionEligibilitiesAttrName' => 'session_eligibilities'
      'eligibilitiesAttrKey' => 'einfracz',
      'assurancePrefix' => 'assuranceprefix',
  ],
  ```
