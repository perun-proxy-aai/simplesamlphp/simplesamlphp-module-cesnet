<?php

declare(strict_types=1);

namespace SimpleSAML\Module\cesnet\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;

/**
 * Class IsEinfraCZEligible.
 *
 * This class puts the current timestamp to attribute storing user eligibilities,
 * when user's affiliations are sufficient to be eligible, which is checked with
 * a configuration that is periodically fetched from configured URL.
 */
class IsEinfraCZEligible extends ProcessingFilter
{
    public const LOGGER_PREFIX = 'cesnet:IsEinfraCZEligible - ';

    public const IDP_ELIGIBLE_AFFILIATIONS_LIST_LINK =
        'https://gitlab.ics.muni.cz/perun/deployment/e-infracz_idps_data/-/raw/main/einfra_isacademic/result.yaml';

    public const LINK_REFRESH_RATE_MINS = 15; // older file will be refreshed

    public const FILE_MAX_AGE_HOURS = 24; // older file won't be considered valid

    public const DATA_FILEPATH = '/tmp/einfra_isacademic_eligible.yml'; // where file will be stored and loaded from

    public const USER_ELIGIBILITY_ATTR = 'userEligibilityAttrName';

    public const USER_AFFILIATIONS_ATTR = 'userAffiliationAttrName';

    public const USER_ELIGIBILITY_KEY = 'eligibilitiesAttrKey';

    private $userAffiliationAttr = 'externalAffiliation';

    private $userEligibilityAttr = 'session_eligibilities';

    private $userEligibilityAttrKey = 'einfracz';


    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $config = Configuration::loadFromArray($config);

        if ($config === null) {
            throw new Exception(
                self::LOGGER_PREFIX . ' configuration is missing or invalid!'
            );
        }

        $this->userEligibilityAttr = $config->getString(self::USER_ELIGIBILITY_ATTR, $this->userEligibilityAttr);
        $this->userAffiliationAttr = $config->getString(self::USER_AFFILIATIONS_ATTR, $this->userAffiliationAttr);
        $this->userEligibilityAttrKey = $config->getString(self::USER_ELIGIBILITY_KEY, $this->userEligibilityAttrKey);
    }

    public function process(&$request)
    {
        $userScopedAffiliations = [];
        if (!empty($request['Attributes'][$this->userAffiliationAttr])) {
            $userScopedAffiliations
                = $request['Attributes'][$this->userAffiliationAttr];
        } else {
            Logger::error(
                self::LOGGER_PREFIX .
                'Attribute with name \'' . $this->userAffiliationAttr . '\' was not received from IdP!'
            );
        }

        $isEligible = $this->isEligible($request['saml:sp:IdP'], $userScopedAffiliations);

        Logger::debug(
            self::LOGGER_PREFIX . 'User is' . ($isEligible ? ' ' : ' not ') . 'eligible'
        );

        if ($isEligible) {
            $request['Attributes'][$this->userEligibilityAttr][$this->userEligibilityAttrKey] = strval(time());
        }
        Logger::debug(
            self::LOGGER_PREFIX . 'Result eligibilities attribute: ' .
            json_encode($request['Attributes'][$this->userEligibilityAttr] ?? 'none')
        );
    }

    private function isEligible(string $idpEntityId, array $userScopedAffiliations): bool
    {
        $userAffiliations = array_map(
            function ($item) {
                return explode("@", $item)[0];
            },
            $userScopedAffiliations
        );

        $allowedAffiliations =  $this->getAllowedAffiliations($idpEntityId);
        $result = array_intersect($userAffiliations, $allowedAffiliations);
        Logger::debug(
            self::LOGGER_PREFIX .
            'Affiliations that make user eligible: ' . implode(",", $result)
        );
        return !empty($result);
    }


    private function getAllowedAffiliations(string $idpEntityId): array
    {
        $allowedAffiliations = [];
        $eligibilities = $this->getEligibilityData();

        if (array_key_exists($idpEntityId, $eligibilities)) {
            $allowedAffiliations = $eligibilities[$idpEntityId];
        }

        return $allowedAffiliations;
    }


    private function getEligibilityData(): array
    {
        $data = 0;
        if (file_exists(self::DATA_FILEPATH)) {
            $file_age = strtotime(date("F d Y H:i:s.", filectime(self::DATA_FILEPATH)));
            if ($file_age < strtotime("-" . self::LINK_REFRESH_RATE_MINS  . " minutes")) {
                $data = $this->fetchAffiliationData();
            }

            if (empty($data) && $file_age > strtotime("-" . self::FILE_MAX_AGE_HOURS . " hours")) {
                $data = $this->readDataFromCacheFile();
            }
        } else {
            $data = $this->fetchAffiliationData();
        }
        return $data;
    }

    private function readDataFromCacheFile(): array
    {
        try {
            $fileSize = filesize(self::DATA_FILEPATH);

            $file = fopen(self::DATA_FILEPATH, "r");
            $sourceData = fread($file, $fileSize);
            fclose($file);

            return yaml_parse($sourceData);
        } catch (\Exception $exception) {
            throw new \Exception(
                self::LOGGER_PREFIX . "Cannot read local stored cache file: "
                . self::DATA_FILEPATH
            );
        }
    }

    private function fetchAffiliationData(): array
    {
        $sourceData = file_get_contents(self::IDP_ELIGIBLE_AFFILIATIONS_LIST_LINK);
        if (!$sourceData) {
            Logger::debug(
                self::LOGGER_PREFIX . "Cannot download data from " . self::IDP_ELIGIBLE_AFFILIATIONS_LIST_LINK
            );
            return [];
        }

        // Store to file
        try {
            $file = fopen(self::DATA_FILEPATH, "w");
            fwrite($file, $sourceData);
            fclose($file);
        } catch (\Exception $exception) {
            Logger::debug(
                self::LOGGER_PREFIX . "Cannot store data to file" . self::DATA_FILEPATH
            );
        }

        return yaml_parse($sourceData);
    }
}
